# Iranian Shipping Price
`aghandeh/iran-shipping-price` is a php package which helps you to calculate cart shipping price for pishtaz and sefareshi.

# How to install?
`composer require aghandeh/iran-shipping-price`
# How to use?

```sh
  
    //first insantiate the shipping class
    $shipping = new \Aghandeh\IranShippingPrice\Shipping();
    
    //source = "TEH" tehran  , destination postal code: 4471965378 , Detination = "GIL" , gilan
    $shipping->setCart('GIL' , 4471965378 , 'TEH');

    //assume that total weight of the package would be 1000 gram
    $shipping->addWeight(1000); //weight must be in gram
    
    //get parcel price
    $shipping->getPishtazPrice();
    $shipping->getSefareshiPrice();
```
# Provinces Short Names
In this package we use source and destination privinces to calculate the shipping. You can use following short-names for provinces.
```sh
        "آذربایجان شرقی" = "EAZ";
        "آذربایجان غربی" = "WAZ";
        "اردبیل" = "ADL";
        "اصفهان" = "ESF";
        "البرز" = "ABZ";
        "ایلام" = "ILM";
        "بوشهر" = "BHR";
        "تهران" = "THR";
        "چهارمحال وبختیاری" = "CHB";
        "چهارمحال و بختیاری" = "CHB";
        "خراسان جنوبی" = "SKH";
        "خراسان رضوی" = "RKH";
        "خراسان شمالی" = "NKH";
        "خوزستان" = "KHZ";
        "زنجان" = "ZJN";
        "سمنان" = "SMN";
        "سیستان وبلوچستان" = "SBN";
        "سیستان و بلوچستان" = "SBN";
        "فارس" = "FRS";
        "قزوین" = "GZN";
        "قم" = "QHM";
        "کردستان" = "KRD";
        "کرمان" = "KRN";
        "کرمانشاه" = "KRH";
        "کهگیلویه وبویراحمد" = "KBD";
        "کهگیلویه و بویراحمد" = "KBD";
        "گلستان" = "GLS";
        "گیلان" = "GIL";
        "لرستان" = "LRS";
        "مازندران" = "MZN";
        "مرکزی" = "MKZ";
        "هرمزگان" = "HRZ";
        "همدان" = "HDN";
        "یزد" = "YZD";
```
